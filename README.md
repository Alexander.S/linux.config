Linux configs repository

ENVIRONMENT

Imported:
.git-completion.bash
aliases for git, etc.
hooks for git commits
doc/ - simple help files for ramdisk creation, tar.bz2, etc.
/etc/udev/rules.d:
lauterbach rule
android rule
/etc/sudoers - sudo timeout change to 2h
/etc/inputrc - neverending history

VIM

cscope preferences
Added 'NERD Tree' plugin, 'taglist' plugin, 'a' plugin.
Red line numbers
Tabulations replaced by '---->', whitespaces shows by red
Removed wrapping

MIDNIGHT COMMANDER

Full transparent theme implemented. For user and root

SCRIPTS

build-gcc script

BACKGROUNDS/WALLPAPERS

Ported backgrounds from Ubuntu 11.04
Different Ubuntu community wallpapers