alias ..='cd ..'
alias a='ls -a'
alias c='cat'
alias v='vim'
alias m='mc'
alias arm='make ARCH=arm -j4'
alias rearm='make ARCH=arm clean ; make ARCH=arm -j4'
alias oboot='usbboot -f'
alias ram_unpack='gunzip -c ../ramdisk.img | cpio -i'
alias ram_pack='find . | cpio -o -H newc | gzip > ../ramdisk.img'
alias show='git show'
alias blame='git blame'
alias br='git branch -a'
alias checkout='git checkout'
alias one='git log --oneline'
alias am='git am'
alias amend='git commit --amend'
alias format='git format-patch -1'
alias add='git add'
alias lg="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias lgr="git log --color --reverse --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gsend='git send-email --smtp-server /usr/bin/msmtp --from "${USER_REAL_NAME} <${TI_MAIL}>" --envelope-sender="${USER_REAL_NAME} <${TI_MAIL}>" --suppress-cc=all'
alias gsendcompose='git send-email --smtp-server /usr/bin/msmtp --from "${USER_REAL_NAME} <${TI_MAIL}>" --envelope-sender="${USER_REAL_NAME} <${TI_MAIL}>" --suppress-cc=all --compose'
alias cpio_pack='find . | cpio -o -H newc > ../newramdisk.cpio'
alias cpio_unpack='cat ../ramdisk.cpio | cpio -i'
alias swapon='sudo swapon -a'
alias swapoff='sudo swapoff -a'
